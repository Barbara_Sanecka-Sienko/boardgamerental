import org.postgresql.ds.PGSimpleDataSource;

import java.sql.*;

public class BDCreator {

    public static void main(String[] args) {
        String dbUrl = "jdbc:postgresql://localhost:5432/boardgame";
        String dbUserName = "postgres";
        String dbUserPassword = "kloto_24";

        // sprawdzenie połączenia z db za pomocą intefrejsu PGSimpleDataSource:
        PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setURL(dbUrl);
        ds.setUser(dbUserName);
        ds.setPassword(dbUserPassword);

        try (Connection connection = ds.getConnection()) {
            createABoardGamesTable(connection);
            createAUsersTable(connection);
            createARentalBoardGamesTable(connection);
            //addRecordsToBGTable(connection);
            //addRecordsToUserTable(connection);
            updateOneOfRecord(connection);
            showAllBoardGames(connection);
            showAllUsers(connection);
            showAllRents(connection);
            findByTitle(connection, "Nemesis");
            findByAuthor(connection, "Adam Kwapinski");
            findByPublisher(connection, "REBEL");
            deleteRecord(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void createABoardGamesTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        String createBoardGamesTable = "CREATE TABLE IF NOT EXISTS board_games(" +
                "ID SERIAL," +
                "title VARCHAR(255), " +
                "author VARCHAR(64), " +
                "publisher VARCHAR(64), " +
                "forMin INT, " +
                "forMax INT, " +
                "age INT, " +
                "minTime INT, " +
                "maxTime INT, " +
                "yearOfRelease INT, " +
                "status VARCHAR(64), " +
                "barcode BIGINT, " +
                "PRIMARY KEY(ID));";
        //String deleteTable = "DROP TABLE board_games;";
        statement.execute(createBoardGamesTable);
    }

    private static void createAUsersTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        String createUsersTable = "CREATE TABLE IF NOT EXISTS users(" +
                "ID SERIAL," +
                "name VARCHAR(64), " +
                "surname VARCHAR(64), " +
                "phone VARCHAR(64), " +
                "mail VARCHAR(64), " +
                "PRIMARY KEY(ID));";
        //String deleteTable = "DROP TABLE users;";
        statement.execute(createUsersTable);
    }

    private static void createARentalBoardGamesTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        String createRentalBoardGamesTable = "CREATE TABLE IF NOT EXISTS rental_bg(" +
                "ID SERIAL," +
                "bg_id INT, " +
                "user_id INT, " +
                "PRIMARY KEY(ID), " +
                "FOREIGN KEY(bg_id) REFERENCES board_games(ID), " +
                "FOREIGN KEY(user_id) REFERENCES users(ID));";

        //String deleteTable = "DROP TABLE rental_bg;";
        statement.execute(createRentalBoardGamesTable);
    }

    private static void addRecordsToBGTable(Connection connection) throws SQLException {
        String title = "Herosi";
        String author = "Adam Kwapinski";
        String publisher = PublishingHouse.REBEL.name();
        int forMin = 2;
        int forMax = 4;
        int age = 8;
        int minTime = 30;
        int maxTime = 60;
        int year = 2012;
        String status = "aktywna";
        long barcode = 1234567890987L;
        PreparedStatement statement = connection.prepareStatement("INSERT INTO board_games VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        statement.setString(1, title);
        statement.setString(2, author);
        statement.setString(3, publisher);
        statement.setInt(4, forMin);
        statement.setInt(5, forMax);
        statement.setInt(6, age);
        statement.setInt(7, minTime);
        statement.setInt(8, maxTime);
        statement.setInt(9, year);
        statement.setString(10, status);
        statement.setLong(11, barcode);
        statement.executeUpdate();
    }

    private static void addRecordsToUserTable(Connection connection) throws SQLException {
        String name = "Alona";
        String surname = "Sustep";
        int phone = 334556778;
        String mail = "sus@gmail.com";
        PreparedStatement statement = connection.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?, ?, ?, ?);");
        statement.setString(1, name);
        statement.setString(2, surname);
        statement.setInt(3, phone);
        statement.setString(4, mail);
        statement.executeUpdate();
    }

    private static void updateOneOfRecord(Connection connection) throws SQLException {
        //zaktualizuj jeden rekord PreparedStatement
        String nameOfColumn = "title";
        String modify = "Nemesis";
        int idToUpdate = 1;
        String update = "UPDATE board_games SET " + nameOfColumn + " = '" + modify + "' WHERE ID = " + idToUpdate + ";";
        PreparedStatement statement = connection.prepareStatement(update);
        statement.executeUpdate();
    }

    private static void deleteRecord(Connection connection) throws SQLException {
        int id = 2;
        String delete = "DELETE FROM board_games WHERE ID = " + id;
        PreparedStatement statement = connection.prepareStatement(delete);
        statement.executeUpdate();
    }

    private static void findByTitle(Connection connection, String title) throws SQLException {
        String find = "SELECT * FROM board_games WHERE title like '" + title + "';";
        PreparedStatement statement = connection.prepareStatement(find);
        ResultSet resultSet = statement.executeQuery();
        System.out.println("BOARD GAMES FOUND BY TITLE: " + title);
        while (resultSet.next()) {
            System.out.print("ID: " + resultSet.getInt(1) + ", ");
            System.out.print("Title: " + resultSet.getString(2) + ", ");
            System.out.print("Author: " + resultSet.getString(3) + ", ");
            System.out.print("Publisher: " + resultSet.getString(4) + ", ");
            System.out.print("For min: " + resultSet.getInt(5) + ", ");
            System.out.print("For max: " + resultSet.getInt(6) + ", ");
            System.out.print("Age: " + resultSet.getInt(7) + ", ");
            System.out.print("Min time: " + resultSet.getInt(8) + ", ");
            System.out.print("Max time: " + resultSet.getInt(9) + ", ");
            System.out.print("Year: " + resultSet.getInt(10) + ", ");
            System.out.print("Status: " + resultSet.getString(11) + ", ");
            System.out.println("Barcode: " + resultSet.getLong(12));
        }
    }

    private static void findByAuthor(Connection connection, String author) throws SQLException {
        String find = "SELECT * FROM board_games WHERE author like '" + author + "';";
        PreparedStatement statement = connection.prepareStatement(find);
        ResultSet resultSet = statement.executeQuery();
        System.out.println("BOARD GAMES FOUND BY AUTHOR: " + author);
        while (resultSet.next()) {
            System.out.print("ID: " + resultSet.getInt(1) + ", ");
            System.out.print("Title: " + resultSet.getString(2) + ", ");
            System.out.print("Author: " + resultSet.getString(3) + ", ");
            System.out.print("Publisher: " + resultSet.getString(4) + ", ");
            System.out.print("For min: " + resultSet.getInt(5) + ", ");
            System.out.print("For max: " + resultSet.getInt(6) + ", ");
            System.out.print("Age: " + resultSet.getInt(7) + ", ");
            System.out.print("Min time: " + resultSet.getInt(8) + ", ");
            System.out.print("Max time: " + resultSet.getInt(9) + ", ");
            System.out.print("Year: " + resultSet.getInt(10) + ", ");
            System.out.print("Status: " + resultSet.getString(11) + ", ");
            System.out.println("Barcode: " + resultSet.getLong(12));
        }
    }

    private static void findByPublisher(Connection connection, String publisher) throws SQLException {
        String find = "SELECT * FROM board_games WHERE publisher like '" + publisher + "';";
        PreparedStatement statement = connection.prepareStatement(find);
        ResultSet resultSet = statement.executeQuery();
        System.out.println("BOARD GAMES FOUND BY PUBLISHER: " + publisher);
        while (resultSet.next()) {
            System.out.print("ID: " + resultSet.getInt(1) + ", ");
            System.out.print("Title: " + resultSet.getString(2) + ", ");
            System.out.print("Author: " + resultSet.getString(3) + ", ");
            System.out.print("Publisher: " + resultSet.getString(4) + ", ");
            System.out.print("For min: " + resultSet.getInt(5) + ", ");
            System.out.print("For max: " + resultSet.getInt(6) + ", ");
            System.out.print("Age: " + resultSet.getInt(7) + ", ");
            System.out.print("Min time: " + resultSet.getInt(8) + ", ");
            System.out.print("Max time: " + resultSet.getInt(9) + ", ");
            System.out.print("Year: " + resultSet.getInt(10) + ", ");
            System.out.print("Status: " + resultSet.getString(11) + ", ");
            System.out.println("Barcode: " + resultSet.getLong(12));
        }
    }

    private static void rentAGame(Connection connection) {
        String getABoardGameID = "SELECT ID FROM ";
    }

    private static void showAllBoardGames(Connection connection) throws SQLException {
        String print = "SELECT * FROM board_games";
        PreparedStatement statement = connection.prepareStatement(print);
        ResultSet resultSet = statement.executeQuery();
        System.out.println("BOARD GAMES: ");
        while (resultSet.next()) {
            System.out.print("ID: " + resultSet.getInt(1) + ", ");
            System.out.print("Title: " + resultSet.getString(2) + ", ");
            System.out.print("Author: " + resultSet.getString(3) + ", ");
            System.out.print("Publisher: " + resultSet.getString(4) + ", ");
            System.out.print("For min: " + resultSet.getInt(5) + ", ");
            System.out.print("For max: " + resultSet.getInt(6) + ", ");
            System.out.print("Age: " + resultSet.getInt(7) + ", ");
            System.out.print("Min time: " + resultSet.getInt(8) + ", ");
            System.out.print("Max time: " + resultSet.getInt(9) + ", ");
            System.out.print("Year: " + resultSet.getInt(10) + ", ");
            System.out.print("Status: " + resultSet.getString(11) + ", ");
            System.out.print("Barcode: " + resultSet.getLong(12) + "\n");
        }
    }

    private static void showAllUsers(Connection connection) throws SQLException {
        String print = "SELECT * FROM users";
        PreparedStatement statement = connection.prepareStatement(print);
        ResultSet resultSet = statement.executeQuery();
        System.out.println("USERS: ");
        while (resultSet.next()) {
            System.out.print("ID: " + resultSet.getInt(1) + ", ");
            System.out.print("Name: " + resultSet.getString(2) + ", ");
            System.out.print("Surname: " + resultSet.getString(3) + ", ");
            System.out.print("Phone: " + resultSet.getInt(4) + ", ");
            System.out.print("Mail: " + resultSet.getString(5) + "\n");
        }
    }

    private static void showAllRents(Connection connection) throws SQLException {
        String print = "SELECT * FROM rental_bg";
        PreparedStatement statement = connection.prepareStatement(print);
        ResultSet resultSet = statement.executeQuery();
        System.out.println("RENTS: ");
        while (resultSet.next()) {
            System.out.print("ID: " + resultSet.getInt(1) + ", ");
            System.out.print("Board game ID: " + resultSet.getInt(2) + ", ");
            System.out.print("User ID: " + resultSet.getInt(3) + "\n");
        }
    }
}
