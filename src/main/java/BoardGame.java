import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BoardGame {

    private int ID;
    private String title;
    private String author;
    private String publishingHouse;
    private int minCountOfPlayers;
    private int maxCountOfPlayers;
    private int playerAge;
    private int minGameTime;
    private int maxGameTime;
    private int yearOfRelease;
    private String status;
    private long barcode;

    @Override
    public String toString() {
        return "BoardGame{" +
                "ID=" + ID +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publishingHouse='" + publishingHouse + '\'' +
                ", minCountOfPlayers=" + minCountOfPlayers +
                ", maxCountOfPlayers=" + maxCountOfPlayers +
                ", playerAge=" + playerAge +
                ", minGameTime=" + minGameTime +
                ", maxGameTime=" + maxGameTime +
                ", yearOfRelease=" + yearOfRelease +
                ", status='" + status + '\'' +
                ", barcode=" + barcode +
                '}';
    }
}
